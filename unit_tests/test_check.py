# Copyright (c) 2018  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

"""
A set of tests for the check_service functions.
"""


import unittest

import mock

import check_service


TEST_SERVICES = ['wicd', 'pppd-dns', 'upower', 'meow']


TEST_RESULTS = {
    'systemd': {
        'wicd': 'enabled',
        'pppd-dns': 'enabled',
        'upower': 'disabled',
        'meow': 'not-installed',
    },

    'sysv': {
        'wicd': 'enabled',
        'pppd-dns': 'disabled',
        'upower': 'not-installed',
        'meow': 'not-installed',
    },
}


LIST_UNIT_FILES_OUTPUT = '''
packagekit.service                         static                                                                     
phpsessionclean.service                    static                                                                     
polkit.service                             static                                                                     
pppd-dns.service                           enabled                                                                    
procps.service                             static                                                                     
quotaon.service                            static                                                                     
rc-local.service                           static                                                                     
umountfs.service                           masked                                                                     
umountnfs.service                          masked                                                                     
umountroot.service                         masked                                                                     
upower.service                             disabled                                                                   
urandom.service                            static                                                                     
usb_modeswitch@.service                    static                                                                     
usbmuxd.service                            static                                                                     
user-runtime-dir@.service                  static                                                                     
user@.service                              static                                                                     
wacom-inputattach@.service                 static                                                                     
wicd.service                               enabled                                                                    
wpa_supplicant-wired@.service              disabled                                                                   
wpa_supplicant.service                     enabled                                                                    
wpa_supplicant@.service                    disabled
'''  # noqa


EXISTANT_FILES = [
    '/etc/init.d/pppd-dns',
    '/etc/init.d/wicd',
]


LISTDIR_OUTPUT = [
    'K12meow',
    'S22ssh',
    'S81speech-dispatcher',
    'S54sudo',
    'S70xdm',
    'S83wicd',
]


class TestSystemd(unittest.TestCase):
    """ Test the helper functions only. """

    @mock.patch('os.path.isdir')
    def test_is_systemd(self, isdir):
        # type: (TestSystemd, mock.MagicMock) -> None
        """ Pretty trivial, really. """

        isdir.return_value = False
        self.assertEqual(check_service.is_systemd(), False)
        isdir.assert_called_once_with('/lib/systemd/system')

        isdir.return_value = True
        self.assertEqual(check_service.is_systemd(), True)
        isdir.assert_called_with('/lib/systemd/system')
        self.assertEqual(isdir.call_count, 2)

    @mock.patch('subprocess.check_output')
    def test_check_service_systemd(self, check_output):
        # type: (TestSystemd, mock.MagicMock) -> None
        """ Test the analysis of systemd --list-unit-files. """

        check_output.return_value = LIST_UNIT_FILES_OUTPUT.encode('UTF-8')
        for svc in TEST_SERVICES:
            self.assertEqual(check_service.check_service_systemd(svc),
                             TEST_RESULTS['systemd'][svc])

        self.assertEqual(check_output.call_count, len(TEST_SERVICES))


class TestSysV(unittest.TestCase):
    """ Test the SysV service checker. """

    @mock.patch('subprocess.check_output')
    def test_get_runlevel(self, check_output):
        # type: (TestSysV, mock.MagicMock) -> None
        """ Test that get_runlevel() parses the output of runlevel. """
        check_output.return_value = 'N 42\n'.encode('UTF-8')
        self.assertEqual(check_service.get_runlevel(), 42)
        check_output.assert_called_with(['runlevel'])
        self.assertEqual(check_output.call_count, 1)

        check_output.return_value = '41 43\n'.encode('UTF-8')
        self.assertEqual(check_service.get_runlevel(), 43)
        check_output.assert_called_with(['runlevel'])
        self.assertEqual(check_output.call_count, 2)

        check_output.return_value = ''.encode('UTF-8')
        self.assertRaises(AssertionError, check_service.get_runlevel)
        check_output.assert_called_with(['runlevel'])
        self.assertEqual(check_output.call_count, 3)

        check_output.return_value = 'N 42\nN 42\n'.encode('UTF-8')
        self.assertRaises(AssertionError, check_service.get_runlevel)
        check_output.assert_called_with(['runlevel'])
        self.assertEqual(check_output.call_count, 4)

        check_output.return_value = 'N M\n'.encode('UTF-8')
        self.assertRaises(ValueError, check_service.get_runlevel)
        check_output.assert_called_with(['runlevel'])
        self.assertEqual(check_output.call_count, 5)

    @mock.patch('os.listdir')
    @mock.patch('os.path.isfile')
    def test_check_service_sysv(self, isfile, listdir):
        # type: (TestSysV, mock.MagicMock, mock.MagicMock) -> None
        """ Test the analysis of /etc/rcX.d. """
        isfile.return_value = False
        self.assertEqual(check_service.check_service_sysv(5, 'foo'),
                         'not-installed')
        isfile.assert_called_with('/etc/init.d/foo')
        self.assertEqual(isfile.call_count, 1)
        self.assertEqual(listdir.call_count, 0)

        isfile.return_value = True
        listdir.return_value = ['S10moo', 'K95foo']
        self.assertEqual(check_service.check_service_sysv(5, 'foo'),
                         'disabled')
        isfile.assert_called_with('/etc/init.d/foo')
        self.assertEqual(isfile.call_count, 2)
        listdir.assert_called_with('/etc/rc5.d')
        self.assertEqual(listdir.call_count, 1)

        isfile.return_value = True
        listdir.return_value = ['S10moo', 'S33foo']
        self.assertEqual(check_service.check_service_sysv(3, 'foo'),
                         'enabled')
        isfile.assert_called_with('/etc/init.d/foo')
        self.assertEqual(isfile.call_count, 3)
        listdir.assert_called_with('/etc/rc3.d')
        self.assertEqual(listdir.call_count, 2)


class TestFull(unittest.TestCase):
    """ Test the full check_services() function. """

    @mock.patch('os.listdir')
    @mock.patch('os.path.isdir')
    @mock.patch('os.path.isfile')
    @mock.patch('subprocess.check_output')
    def test_check_services(self,           # type: TestFull
                            check_output,   # type: mock.MagicMock
                            isfile,         # type: mock.MagicMock
                            isdir,          # type: mock.MagicMock
                            listdir         # type: mock.MagicMock
                            ):              # type: (...) -> None
        """ Test the all-in-one function. """
        # systemd first
        isdir.return_value = True
        check_output.return_value = LIST_UNIT_FILES_OUTPUT.encode('UTF-8')
        self.assertEqual(dict(check_service.check_services(TEST_SERVICES)),
                         TEST_RESULTS['systemd'])
        isdir.assert_called_with('/lib/systemd/system')
        self.assertEqual(isdir.call_count, 1)
        self.assertEqual(check_output.call_count, len(TEST_SERVICES))
        self.assertEqual(isfile.call_count, 0)
        self.assertEqual(listdir.call_count, 0)

        # now SysV
        def mock_isfile(fname):
            # type: (str) -> bool
            """ Mock os.path.isfile() for our services. """
            return fname in EXISTANT_FILES

        isdir.return_value = False
        isfile.side_effect = mock_isfile
        check_output.return_value = 'N 5\n'.encode('UTF-8')
        listdir.return_value = LISTDIR_OUTPUT
        self.assertEqual(dict(check_service.check_services(TEST_SERVICES)),
                         TEST_RESULTS['sysv'])
        isdir.assert_called_with('/lib/systemd/system')
        self.assertEqual(isdir.call_count, 2)
        self.assertEqual(isfile.call_count, len(TEST_SERVICES))
        check_output.assert_called_with(['runlevel'])
        self.assertEqual(check_output.call_count, len(TEST_SERVICES) + 1)
        listdir.assert_called_with('/etc/rc5.d')
        self.assertEqual(listdir.call_count, len(EXISTANT_FILES))
