#!/usr/bin/python
#
# Copyright (c) 2018  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

"""
Check whether the specified system services are installed and enabled.
"""


from __future__ import print_function

import os
import subprocess
import sys

from typing import List, Tuple


_TYPING_USED = (List, Tuple)


def is_systemd():
    # type: () -> bool
    """ Check whether we are running under systemd. """
    return os.path.isdir('/lib/systemd/system')


def get_runlevel():
    # type: () -> int
    """ Get the system runlevel (hopefully 1 through 5). """
    out = subprocess.check_output(['runlevel']).decode('UTF-8')
    lines = out.rstrip().split('\n')
    assert len(lines) == 1
    fields = lines[0].split()
    assert len(fields) == 2
    return int(fields[1])


def check_service_systemd(svc):
    # type: (str) -> str
    """ Check whether a systemd service is installed and enabled. """
    raw = subprocess.check_output(['systemctl', 'list-unit-files',
                                   '--no-pager', '--no-legend'])
    lines = raw.decode('UTF-8').rstrip().split('\n')
    svc_name = svc + '.service'
    for line in lines:
        fields = line.split()
        if not fields or fields[0] != svc_name:
            continue
        if len(fields) == 1:
            # Eh?!
            return 'disabled'

        # Let's hope we don't have to check for static services...
        return 'enabled' if fields[1] == 'enabled' else 'disabled'

    return 'not-installed'


def check_service_sysv(runlevel, svc):
    # type: (int, str) -> str
    """ Check whether a SysV service is installed and enabled. """
    if not os.path.isfile('/etc/init.d/' + svc):
        return 'not-installed'

    dirname = '/etc/rc{run}.d'.format(run=runlevel)
    for fname in os.listdir(dirname):
        if fname.startswith('S') and fname[3:] == svc:
            return 'enabled'

    return 'disabled'


def check_services(names):
    # type: (List[str]) -> List[Tuple[str, str]]
    """ Return a list of (service-name, status) tuples. """
    under_systemd = is_systemd()
    if under_systemd:
        check = check_service_systemd
    else:
        runlevel = get_runlevel()

        def check_sysv(svc):
            # type: (str) -> str
            """ Check a SysV service under the current runlevel. """
            return check_service_sysv(runlevel, svc)

        check = check_sysv

    return [(svc, check(svc)) for svc in names]


def main():
    # type: () -> None
    """ Parse command-line arguments, check for services. """
    if len(sys.argv) < 2:
        exit('Usage: check_service service-name...')

    for svc, status in check_services(sys.argv[1:]):
        print('{svc}\t{status}'.format(svc=svc, status=status))


if __name__ == '__main__':
    main()
